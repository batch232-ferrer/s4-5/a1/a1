import com.zuitt.example.Contact;
import com.zuitt.example.Phonebook;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        // [PERSON 1]
        // Setters
        Contact person1 = new Contact();
        person1.setName("John Doe");
        person1.setContactNumber("+639152468596");
        person1.setAddress("Quezon City");

        // [PERSON 2]
        // Setters
        Contact person2 = new Contact();
        person2.setName("Jane Doe");
        person2.setContactNumber("+639162148573");
        person2.setAddress("Caloocan City");

        // [ADDING CONTACTS TO THE PHONEBOOK]
        phonebook.contacts.add(person1.getName());
        phonebook.contacts.add(person2.getName());

        if(phonebook.getContacts().isEmpty()){
            System.out.println("Phonebook is currently empty. Please add a contact");
        } else {
            // Getters for person1
            System.out.println(person1.getName());
            System.out.println("----------------");
            System.out.println(person1.getName() + " has the following registered number:");
            System.out.println(person1.getContactNumber());
            System.out.println(person1.getName() + " has the following registered address:");
            System.out.println("I live in " + person1.getAddress());

            // Getters for person2
            System.out.println(person2.getName());
            System.out.println("----------------");
            System.out.println(person2.getName() + " has the following registered number:");
            System.out.println(person2.getContactNumber());
            System.out.println(person2.getName() + " has the following registered address:");
            System.out.println("I live in " + person2.getAddress());
        }

    }
}