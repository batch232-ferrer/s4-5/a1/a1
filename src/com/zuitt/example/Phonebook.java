package com.zuitt.example;

import java.util.ArrayList;

public class Phonebook {
    // [PROPERTIES]
    public ArrayList<String> contacts = new ArrayList<>();

    // [CONSTRUCTORS]
    public Phonebook(ArrayList<String> contacts){
        this.contacts = contacts;
    }

    public Phonebook(){};

    // [SETTERS AND GETTERS]
        // [Getter]
    public ArrayList<String> getContacts(){
        return this.contacts;
    }

        // [Setter]
    public void setContacts(ArrayList<String> contacts){
        this.contacts = contacts;
    }
}
